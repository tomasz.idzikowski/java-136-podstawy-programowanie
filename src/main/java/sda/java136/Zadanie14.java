package sda.java136;

//14.Napisz program, który dla podanej liczby wyświetla jej liczbę dzielników.
public class Zadanie14 {
    public static void main(String[] args) {
        int number = 12;
        System.out.println(countDividers(number));
    }

    public static int countDividers(int number) {
        int counter = 2;
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                counter++;
            }
        }
        return counter;
    }
}
