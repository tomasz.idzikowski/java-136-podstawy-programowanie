package sda.java136;

//17.Napisz program, który sprawdzi z ilu małych liter składa się wyraz.
public class Zadanie17 {
    public static void main(String[] args) {
        String sentence = "Wyraz";
        int result = getSmallLetterNumber(sentence);
        int result2 = getSmallLetterNumber2(sentence);
        System.out.println("Wynik " + result);
        System.out.println("Wynik " + result2);
    }

    public static int getSmallLetterNumber(String sentence) {
        int counter = 0;
        for (int i = 0; i < sentence.length(); i++) {
            if (sentence.charAt(i) <= 'z' && sentence.charAt(i) >= 'a') {
                counter++;
            }
        }
        return counter;
    }

    public static int getSmallLetterNumber2(String sentence) {
        int counter = 0;
        for (int i = 0; i < sentence.length(); i++) {
            if (Character.isLowerCase(sentence.charAt(i))) {
                counter++;
            }
        }

        return counter;
    }
}
