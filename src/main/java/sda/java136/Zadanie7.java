package sda.java136;

import java.util.Scanner;

//7. Napisz metodę, która zapyta usera o liczbę.
// Jeśli liczba jest równa 5 lub 10 lub 15 poda odpowiedni komunikat. Zastosować instrukcję switch.
public class Zadanie7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe");
        int liczba = scanner.nextInt();
        switch (liczba) {
            case 5: case 10: case 15:
                System.out.println("Podano " + liczba);
                break;
            default:
                System.out.println("Niepoprawna liczba");
        }
    }
}
