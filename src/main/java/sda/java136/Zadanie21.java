package sda.java136;

import java.util.Scanner;

//21.Napisz program, który pobierze od usera dodatnią liczbę i wypisze wszystkie liczby naturalne mniejsze od tej liczby zgodnie z regułami
//a) jeśli liczba jest podzielna przez 3, napisze komunikat "Liczba jest podzielna przez 3"
//b) jeśli liczba jest podzielna przez 5, napisze komunikat "Liczba jest podzielna przez 5"
//a jeśli liczba jest podzielna przez 3 i przez 5, napisze komunikat "Liczba jest podzielna przez 15"
public class Zadanie21 {
    public static void main(String[] args) {
        System.out.println("Podaj dodatnią liczbę ");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        scanner.nextLine();
        if (number < 0) {
            System.out.println("Podałeś liczbę ujemną... ");
        } else if (number == 0) {
            System.out.println("Zero nie jest liczbą dodatnią... ");
        } else {
            for (int i = 0; i <= number; i++) {
                if (i % 3 == 0 && i % 5 == 0) {
                    System.out.println(i + " Liczba jest podzielna przez 15");
                } else if (i % 3 == 0) {
                    System.out.println(i + " Liczba jest podzielna przez 3");
                } else if (i % 5 == 0) {
                    System.out.println(i + " Liczba jest podzielna przez 5");
                }
            }
        }
    }
}
