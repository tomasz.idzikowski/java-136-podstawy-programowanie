package sda.java136;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

//37.Napisz program, który będzie umożliwiał tłumaczenie słów z języka polskiego na angielski
public class Zadanie37 {
    public static void main(String[] args) {
        try {
            translateUsingWordFromFile();
        } catch (IOException komputer) {
            System.out.println("Brak pliku");
        }
    }

    public static void translateUsingWordFromFile() throws IOException {
        Map<String, String> dictionary = loadFromFile();
        handleTranslation(dictionary);
    }

    private static void translateUsingPredefinedWords() {
        Map<String, String> dictionary = loadPredefinedWords();
        handleTranslation(dictionary);
    }

    private static Map<String, String> loadFromFile() throws IOException {
        Map<String, String> dictionary = new HashMap<>();
        List<String> plik = Files.readAllLines(Paths.get("slownik.txt"));
        for (String line : plik) {
            String[] array = line.split(",");
            dictionary.put(array[0], array[1]);
        }
        return dictionary;
    }

    private static Map<String, String> loadPredefinedWords() {
        Map<String, String> dictionary = new HashMap<>();
        dictionary.put("pies", "dog");
        dictionary.put("kot", "cat");
        dictionary.put("kon", "horse");
        return dictionary;
    }

    private static void handleTranslation(Map<String, String> dictionary) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj polskie słowo do przetłumaczenia");
        String input = scanner.nextLine();
        if (dictionary.containsKey(input)) {
            System.out.println("Jego tłumaczenie to " + dictionary.get(input));
        } else {
            System.out.println("Nie ma takiego słowa");
        }
    }
}
