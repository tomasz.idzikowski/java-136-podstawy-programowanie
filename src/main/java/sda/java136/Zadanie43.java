package sda.java136;

import java.util.ArrayList;
import java.util.List;

public class Zadanie43 {
    private static final String PLAYER_ONE = "Gracz";
    private static final String PLAYER_TWO = "CPU";
    private static List<KartaDoGry> player1Cards = new ArrayList<>();
    private static List<KartaDoGry> player2Cards = new ArrayList<>();
    private static final KartaDoGry DAMA_KIER = new KartaDoGry(Figura.DAMA, Kolor.KIER);
    private static final KartaDoGry DAMA_PIK = new KartaDoGry(Figura.DAMA, Kolor.PIK);
    private static final KartaDoGry KROL_KIER = new KartaDoGry(Figura.KROL, Kolor.KIER);
    private static final KartaDoGry KROL_KARO = new KartaDoGry(Figura.KROL, Kolor.KARO);
    private static final KartaDoGry WALET_KARO = new KartaDoGry(Figura.WALET, Kolor.KARO);


    public static void prepareCardsForPlayerOne() {
        player1Cards.add(DAMA_KIER);
        player1Cards.add(DAMA_PIK);
        player1Cards.add(KROL_KIER);
    }

    public static void prepareCardsForPlayerTwo() {
        player2Cards.add(KROL_KARO);
        player2Cards.add(WALET_KARO);
    }

    public static void showStatus() {
        System.out.println("-------");
        System.out.println("Stan gry");
        System.out.println(PLAYER_ONE + " ma karty " + player1Cards);
        System.out.println(PLAYER_TWO + " ma karty " + player2Cards);
        System.out.println("-------");
    }

    public static String play() {
        showStatus();
        while (player1Cards.size() != 0 && player2Cards.size() != 0) {
            //TODO cała rozgrywka
            //1
            //K, Q, A
            //Q, W

            System.out.println("Trwa bitwa");
            KartaDoGry p1Card = player1Cards.get(0);
            KartaDoGry p2Card = player2Cards.get(0);
            System.out.println(PLAYER_ONE + " wyciąga kartę " + p1Card);
            System.out.println(PLAYER_TWO + " wyciąga kartę " + p2Card);

            //K compareTo Q

            //list.remove(0)

            //2
            //Q, A, K, Q
            //W

            //Q z W
            //list.remove(0)

            //3
            //A, K, Q, Q, W
            //nic

            if (p1Card.compareTo(p2Card) < 0) {
                System.out.println("Bitwę wygrywa " + PLAYER_TWO);
                player1Cards.remove(0);
                player2Cards.remove(0);
                player2Cards.add(p2Card);
                player2Cards.add(p1Card);
            } else {
                System.out.println("Bitwę wygrywa " + PLAYER_ONE);
                player1Cards.remove(0);
                player2Cards.remove(0);
                player1Cards.add(p2Card);
                player1Cards.add(p1Card);
            }
            showStatus();
        }
        //przegrywa ten gracz, który nie ma już kart
        if (player2Cards.size() == 0) {
            return PLAYER_ONE;
        }
        return PLAYER_TWO;
    }

    public static void main(String[] args) {
        prepareCardsForPlayerOne();
        prepareCardsForPlayerTwo();
        String winner = play();
        System.out.println("Player " + winner + " wins");
    }
}
