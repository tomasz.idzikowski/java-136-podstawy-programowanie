package sda.java136;

//28.Napisać metodę, która przyjmuje stringa postaci 1,2,3,5,10 i sumuje liczby oddzielone przecinkami.
public class Zadanie28 {
    public static void main(String[] args) {
        String text = "1,2,3,5,10";
        int result = getSum(text);
        System.out.println(result);
    }

    private static int getSum(String text) {
        int sum = 0;
        String[] array = text.split(",");
        for (String element : array) {
            sum = sum + Integer.parseInt(element);
        }
        return sum;
    }
}
