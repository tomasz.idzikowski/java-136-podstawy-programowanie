package sda.java136;

import java.util.Comparator;

public class BookComparatorByPrice implements Comparator<Book> {
    public int compare(Book b1, Book b2) {
        return Double.compare(b1.getPages(),b2.getPages());
//        return (int) (b1.getPrice() - b2.getPrice());
    }
}
