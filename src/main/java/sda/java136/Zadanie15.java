package sda.java136;

//15.Napisz program, który sprawdzi czy podana liczba jest sześcianem pewnej liczby całkowitej.
public class Zadanie15 {
    public static void main(String[] args) {
        int number = 8;
        System.out.println(isSquareOfNumbers(number));
    }

    public static boolean isSquareOfNumbers(int number) {
        int i = 1;
        while (i * i * i <= number) {
            if (i * i * i == number) return true;
            i++;
        }
        return false;
    }
}
