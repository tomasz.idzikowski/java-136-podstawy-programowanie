package sda.java136;

public class Car {
    private String marka;
    private String model;
    private int przebieg;

    public Car(String marka, String model, int przebieg) {
        this.marka = marka;
        this.model = model;
        this.przebieg = przebieg;
    }

    @Override
    public String toString() {
        return "Car{" +
                "marka='" + marka + '\'' +
                ", model='" + model + '\'' +
                ", przebieg=" + przebieg +
                '}';
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPrzebieg() {
        return przebieg;
    }

    public void setPrzebieg(int przebieg) {
        if (przebieg >= 0) {
            this.przebieg = przebieg;
        }
        System.out.println("Podałes niepoprawny przebieg");
    }
}
