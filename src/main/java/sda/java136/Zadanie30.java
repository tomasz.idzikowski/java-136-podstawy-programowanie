package sda.java136;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

//30.Napisz program, który pobierze od użytkownika 5 dowolnie dużych liczb (zmiennych typu int) i wypisze te, które wystąpiły minimum dwukrotnie.
public class Zadanie30 {

    public static void calculateWithCollection() throws IOException {
        List<String> plik = Files.readAllLines(Paths.get("dane.txt"));
        Map<Integer, Integer> mapa = new HashMap<>();

        for (String line : plik) {
            int number = Integer.parseInt(line);
            if (mapa.containsKey(number)) {
                Integer value = mapa.get(number);
                mapa.replace(number, value + 1);
            } else {
                mapa.put(number, 1);
            }
        }

        for (Map.Entry<Integer, Integer> entry : mapa.entrySet()) {
            if (entry.getValue() >= 2) {
                System.out.println(entry.getKey());
            }
        }
    }

    public static void calculateWithArray() {
        int[] array = new int[100];
        Scanner scanner = new Scanner(System.in);
        int ileLiczb = 5;

        for (int i = 0; i < ileLiczb; i++) {
            int liczba = scanner.nextInt();
            array[liczba] = array[liczba] + 1;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] >= 2) {
                System.out.println(i);
            }
        }
    }


    public static void main(String[] args) throws IOException {
        calculateWithArray();
        calculateWithCollection();
    }
}
