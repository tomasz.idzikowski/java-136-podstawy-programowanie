package sda.java136;

import java.util.Scanner;

//23.Napisz program, który obliczy liczę samogłosek we wprowadznym tekście.
public class Zadanie23 {
    public static void main(String args[]) {
        int count = 0;
        System.out.println("Wpisz samogłoskę :");
        Scanner sc = new Scanner(System.in);
        String sentence = sc.nextLine();

        count = getVowels(sentence);
        System.out.println("Liczba samogłosek = " + count);
        count = getVowels2(sentence);
        System.out.println("Liczba samogłosek = " + count);
    }

    private static int getVowels(String sentence) {
        int count = 0;
        for (int i = 0; i < sentence.length(); i++) {
            char ch = sentence.charAt(i);
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'y') {
                count++;
            }
        }
        return count;
    }

    private static int getVowels2(String sentence) {
        int count = 0;
        for (int i = 0; i < sentence.length(); i++) {
            char ch = sentence.charAt(i);
            switch (ch) {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                case 'y':
                    count++;
            }
        }
        return count;
    }
}
