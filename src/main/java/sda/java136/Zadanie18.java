package sda.java136;

//18.Napisz program, który sprawdzi czy tekst jest palindromem. Zrobić na dwa sposoby.
public class Zadanie18 {

    public static void main(String[] args) {
        String word = "Ala";
        System.out.println(isPalindrome(word));
        System.out.println(isPalindrome2(word));
        System.out.println(isPalindrome3(word));
    }

    public static boolean isPalindrome(String text) {
        String word = text.toLowerCase();
        for (int i = 0; i < word.length() / 2; i++) {
            if (word.charAt(i) != word.charAt(word.length() - i - 1)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isPalindrome2(String text) {
        String word = text.toLowerCase();
        StringBuilder reversedWord = new StringBuilder(word);
        reversedWord.reverse();
        if (word.equals(reversedWord.toString())) {
            return true;
        }
        return false;
    }

    public static boolean isPalindrome3(String text) {
        return text.toLowerCase().equals(new StringBuilder(text.toLowerCase()).reverse().toString());
    }
}
