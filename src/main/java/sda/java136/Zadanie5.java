package sda.java136;

//5. Napisz program, który wyswietli wszystkie liczby naturalne od 1 do 100.
public class Zadanie5 {
    public static void main(String[] args) {
        for (int i = 1; i <= 100; i++) {
            System.out.println(i);
        }
    }
}
