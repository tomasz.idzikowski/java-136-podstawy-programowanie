package sda.java136;

//9.Napisz metodę, która sprawdza czy liczba jest pierwsza.
public class Zadanie9 {

    public static void main(String[] args) {
        int number = 7;
        boolean numberPrime = isNumberPrime(number);
        boolean numberPrime2 = isNumberPrime2(number);
        System.out.println(numberPrime);
        System.out.println(numberPrime2);

    }

    public static boolean isNumberPrime(int number) {
        int licznikDzielnikow = 0;
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                licznikDzielnikow++;
            }
        }
        if (licznikDzielnikow == 2) {
            return true;
        }
        return false;
    }

    public static boolean isNumberPrime2(int number) {
        for (int i = 2; i <= (int) Math.sqrt(number); i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
