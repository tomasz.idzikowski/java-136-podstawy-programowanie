package sda.java136;

//24.Napisz program, który pobierze od użytkownika jedną liczbę dodatnią (typu int) i obliczy sumę cyfr podanej liczby.
//Zaimplementować dwa sposoby.
public class Zadanie24 {
    public static void main(String[] args) {
        int number = 1258;
        System.out.println("Suma : " + getSumMethod(number));
        System.out.println("Suma : " + getSumMethod2(number));
    }

    public static int getSumMethod(int number) {
        int rest = 0;
        int result = 0;
        while (number != 0) {
            result = result + number % 10;
            number = number / 10;
        }
        return result;
    }

    public static int getSumMethod2(int number) {
        int result = 0;
        String text = Integer.toString(number);
        for (int i = 0; i < text.length(); i++) {
            char ch = text.charAt(i);
            //int digit = Integer.parseInt(Character.toString(ch));
            int digit = Integer.parseInt(ch + "");
            result += digit;
        }
        return result;
    }
}
