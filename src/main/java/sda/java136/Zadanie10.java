package sda.java136;

import java.util.Scanner;

//10.Napisz program, który pobierze od użytkownika liczbę dodatnią i wypisze wszystkie liczby pierwsze, większe od 1 i mniejsze od podanej liczby.
public class Zadanie10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe");
        int liczba = scanner.nextInt();
        for (int i = 2; i < liczba; i++) {
            if (isPrime(i)){
                System.out.println(i);
            }
        }
    }

    public static boolean isPrime(int number) {
        for (int i = 2; i <= (int) Math.sqrt(number); i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
