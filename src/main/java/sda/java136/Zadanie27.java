package sda.java136;

import java.util.Random;

//27.Napisz metodę, która losuje x liczb z przedziału <a,b> gdzie a,b,x są dodatnimi liczbami całkowitymi.
public class Zadanie27 {
    public static void main(String[] args) {
        int x = 6;
        int a = 11;
        int b = 12;
        drawNumbers(x, a, b);
    }

    public static void drawNumbers(int x, int a, int b) {
        Random random = new Random();
        for (int i = 0; i < x; i++) {
            int number = random.nextInt(a, b + 1);
            System.out.println(number);
        }
    }
}

