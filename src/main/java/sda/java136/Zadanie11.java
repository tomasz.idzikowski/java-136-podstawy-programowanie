package sda.java136;

//11.Napisz program, który oblicza silnię dla podanej liczby całkowitej.
public class Zadanie11 {
    public static int silniaIter(int n) {
        if (n == 0 || n == 1) {
            return 1;

        } else {
            int result = 1;
            for (int i = 2; i <= n; i++) {
                result = result * i;
            }
            return result;
        }
    }

    public static int silniaRek(int n) {
        // 1*2*3*4*5
        if (n == 0 || n == 1) {
            return 1;
        }
        return silniaRek(n - 1) * n;
    }

    public static void main(String[] args) {
        System.out.println(silniaIter(5));
        System.out.println(silniaRek(5));
    }
}
