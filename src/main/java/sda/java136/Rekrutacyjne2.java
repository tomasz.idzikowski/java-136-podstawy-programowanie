package sda.java136;

public class Rekrutacyjne2 {
    public static void main(String[] args) {
        int[] tab = {10, 12, 4, 5, 9};
        int result = ArrayChallenge(tab);
        System.out.println(result);
    }

    public static int ArrayChallenge(int[] arr) {
        int indexSpadek = 0;
        int indexWzrost = 0;
        int maxRoznica = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] - arr[i] > maxRoznica) {
                    indexSpadek = i;
                    indexWzrost = j;
                    maxRoznica = arr[j] - arr[i];
                }
            }
        }
        if (maxRoznica == 0) {
            return -1;
        }
        System.out.println("Wartość spadku " + arr[indexSpadek]);
        System.out.println("Wartość wzrostu " + arr[indexWzrost]);
        return maxRoznica;
    }
}
