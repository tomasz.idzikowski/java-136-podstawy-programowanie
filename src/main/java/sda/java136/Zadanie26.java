package sda.java136;

//26.Napisz program, który zapyta się usera o 10 elementów całkowitych, umieści je w tablicy i przezmnoży przez 2, nastepnie znajdzie najmniejszy i największy element.
public class Zadanie26 {
    public static void main(String[] args) {
        int[] array = {11, 23, 4, 7, 87, 54, 9, 12, 54, 23};
        int max = array[0];
        int min = array[0];
        for (int i = 0; i < array.length; i++) {
            array[i] = array[i] * 2;
            if (array[i] > max) {
                max = array[i];
            }
            if (array[i] < min) {
                min = array[i];
            }
        }
        System.out.println("Min " + min);
        System.out.println("Max " + max);
    }
}
