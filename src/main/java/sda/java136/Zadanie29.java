package sda.java136;

//29.Napisz program, który pobierze od użytkownika liczbę dodatnią (typu int) i obliczy liczbę Fibonacciego o wskazanym indeksie. Przykładowo, jeśli użytkownik poda liczbę 5, Twój program powinien wypisać piątą liczbę Fibonacciego.
public class Zadanie29 {
    //Ciag Fibonacciego 1,1,2,3,5,8,13,21,34,...
    public static int fibonacciRek(int n) {
        if (n == 1 || n == 2) {
            return 1;
        }
        return fibonacciRek(n - 1) + fibonacciRek(n - 2);
    }

    public static int fibonacciIter(int n) {
        int a = 1;
        int b = 1;
        if (n == 1 || n == 2) {
            return 1;
        }
        for (int i = 2; i < n; i++) {
            int c = b;
            b = a + b;
            a = c;
        }
        return b;
    }

    public static void main(String[] args) {
        System.out.println(fibonacciIter(3));
        System.out.println(fibonacciIter(4));
        System.out.println(fibonacciRek(3));
        System.out.println(fibonacciRek(4));
    }
}
