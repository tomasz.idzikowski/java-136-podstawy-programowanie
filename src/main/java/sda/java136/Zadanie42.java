package sda.java136;

import java.util.Stack;

public class Zadanie42 {

    public static String getBinaryWithStack(int n) {
        Stack<Integer> stack = new Stack<>();
        StringBuilder result = new StringBuilder("");
        while (n != 0) {
            stack.add(n % 2);
            n = n / 2;
        }
        while (!stack.empty()) {
            Integer element = stack.pop();
            result.append(element);
        }

        return result.toString();
    }

    public static void main(String[] args) {
        System.out.println(getBinaryWithStack(8)); //1000
    }
}
