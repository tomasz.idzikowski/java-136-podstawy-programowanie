package sda.java136;

import java.util.Scanner;

//3. Napisz program, w którym zapytasz usera o dwie liczby, następnie przemnożysz je i wyświetlisz na ekranie.
public class Zadanie3 {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Podaj pierwszą liczbę: ");
        int liczba1 = myScanner.nextInt();
        System.out.println("Podaj drugą liczbę: ");
        int liczba2 = myScanner.nextInt();
        System.out.println("Wynik mnożenia: " + liczba1 * liczba2);
    }
}
