package sda.java136;

//33.Zaprojektuj klasę, która będzie reprezentowała samochód. Utworz konstruktor i zdefiniuj kilka obiektów tej klasy.
//Zaimplementuj setery, gettery oraz metodę toString(). Spróbuj zmienic przebieg samochodu na -100 w metodzie. Pomyśl jak mozna się przed tym zabezpieczyć.
public class Zadanie33 {
    public static void main(String[] args) {
        Car car1 = new Car("Opel","Corsa",100000);
        System.out.println(car1);
        Car car2 = new Car("Ford","Fiesta",50000);
        car2.setPrzebieg(-150000);
        System.out.println(car2);
    }
}
