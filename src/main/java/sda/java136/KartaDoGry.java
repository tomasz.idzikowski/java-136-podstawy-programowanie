package sda.java136;

public class KartaDoGry implements Comparable<KartaDoGry>{
    private Figura figura;
    private Kolor kolor;

    public KartaDoGry(Figura figura, Kolor kolor) {
        this.figura = figura;
        this.kolor = kolor;
    }

    @Override
    public String toString() {
        return "KartaDoGry{" +
                "figura=" + figura +
                ", kolor=" + kolor +
                '}';
    }

    @Override
    public int compareTo(KartaDoGry karta) {
        if (this.figura.equals(karta.figura)){
            return this.kolor.getValue()-karta.kolor.getValue();
        }
       return this.figura.compareTo(karta.figura);
    }
}
