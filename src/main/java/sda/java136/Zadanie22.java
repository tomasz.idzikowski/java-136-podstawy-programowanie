package sda.java136;

import java.util.Scanner;

//22.Napisz program, który wyswietla komunikat "Złe Hasło" dopóki user wpisze właściwe hasło.
public class Zadanie22 {
    public static void main(String[] args) {
        String password = "123456";
        String passwordFromUser = "ASD";
        do {
            System.out.println("Podaj hasło");
            Scanner scanner = new Scanner(System.in);
            passwordFromUser = scanner.nextLine();
        }
        while (!password.equals(passwordFromUser));

        System.out.println("Zapraszamy");
    }
}
