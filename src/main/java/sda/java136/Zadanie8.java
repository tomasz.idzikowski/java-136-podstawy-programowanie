package sda.java136;

import java.util.Scanner;

//8. Napisz metodę, która sprawdzi czy imię podane przez usera jest kobiece.
public class Zadanie8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj dowolne polskie imię: ");
        String name = scanner.nextLine();
        if (name.endsWith("a")) {
            System.out.println("Podane imię jest kobiece");
        } else {
            System.out.println("Podane imię nie jest kobiece");
        }
    }
}
