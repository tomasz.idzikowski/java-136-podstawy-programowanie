package sda.java136;

//6. Napisz program, który wyświetli wszystkie liczby podzielne przez 2 z przedziału <1,1000>
public class Zadanie6 {
    public static void main(String[] args) {
        for (int i = 2; i <= 1000; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }
}
