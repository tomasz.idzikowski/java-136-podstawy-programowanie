package sda.java136;

//13.Napisz program, który sprawdzi czy podana liczba mniejsza niż 100 jest kwadratem pewnej liczby całkowitej.
public class Zadanie13 {
    public static void main(String[] args) {
        int number = 17;
        System.out.println(isSquareOfNumbers(number));
    }

    public static boolean isSquareOfNumbers(int number) {
        int i = 1;
        while (i * i <= number) {
            if (i * i == number) return true;
            i++;
        }
        return false;
    }
}
