package sda.java136;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Zadanie38 {
    public static void main(String[] args) {
        List<Book> books = new ArrayList<>();
        Book book1 = new Book("C", "X", 140, 47.59);
        Book book2 = new Book("F", "Y", 810, 47.99);
        Book book3 = new Book("A", "Z", 30, 9.57);
        books.add(book1);
        books.add(book2);
        books.add(book3);

        System.out.println(book1.compareTo(book2));

        // sortowanie książek

        System.out.println("naturalne sortowanie");
        Collections.sort(books); // naturalne sortowanie, compareTo w klasie Book, komparator wewnętrzny
        System.out.println(books);

        System.out.println("komparator zewnetrzny po pages");
        Collections.sort(books,new BookComparatorByPages());
        System.out.println(books);

        System.out.println("komparator zewnetrzny po price");
        Collections.sort(books,new BookComparatorByPrice());
        System.out.println(books);

        System.out.println("komparator zewnetrzny po title");
        Collections.sort(books,new BookComparatorByTitle());
        System.out.println(books);
    }
}
