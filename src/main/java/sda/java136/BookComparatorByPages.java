package sda.java136;

import java.util.Comparator;

public class BookComparatorByPages implements Comparator<Book> {
    @Override
    public int compare(Book b1, Book b2) {
//        return Integer.compare(b1.getPages(),b2.getPages());
        return b1.getPages() - b2.getPages();
    }
}
