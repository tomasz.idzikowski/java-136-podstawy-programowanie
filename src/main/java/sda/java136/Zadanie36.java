package sda.java136;

import java.util.Arrays;

//36.https://codingbat.com/prob/p198700
public class Zadanie36 {
    public static void main(String[] args) {
        System.out.println(evenlySpaced(2, 4, 6));
        System.out.println(evenlySpaced(4, 6, 2));
        System.out.println(evenlySpaced(4, 6, 3));
    }

    public static boolean evenlySpaced(int a, int b, int c) {
        int[] array = {a, b, c};
        Arrays.sort(array);
        int diff1 = array[1] - array[0];
        int diff2 = array[2] - array[1];
        if (diff1 == diff2) {
            return true;
        }
        return false;
    }
}
