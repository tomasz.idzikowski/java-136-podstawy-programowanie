package sda.java136;

import java.util.Comparator;

public class BookComparatorByTitle implements Comparator<Book> {
    public int compare(Book b1, Book b2) {
        return b1.getTitle().compareTo(b2.getTitle());
    }
}
