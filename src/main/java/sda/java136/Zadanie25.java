package sda.java136;

//25.Napisz metodę, która przyjmuje kilka wyrazów oddzielonych spacją, następnie tworzy nowy wyraz z pierwszych liter wszystkich wyrazów i go zwraca.
public class Zadanie25 {
    public static void main(String[] args) {
        String text = "Ala ma kota";
        System.out.println(createString(text));
        System.out.println(createString2(text));
    }

    public static String createString(String text) {
        String result = "";
        String[] elements = text.split(" ");
        for (String element : elements) {
            result = result + element.charAt(0);
        }
        return result;
    }

    public static String createString2(String text) {
        StringBuilder result = new StringBuilder();
        String[] elements = text.split(" ");

        for (int i = 0; i < elements.length; i++) {
            result.append(elements[i].charAt(0));
        }

//        for (String element : elements) {
//            result.append(element.charAt(0));
//        }

        return result.toString();
    }
}