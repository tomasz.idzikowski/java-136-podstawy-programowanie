package sda.java136;

//12.Napisz program, który wyświetli wszystkie cyfry w podanym tekście.
public class Zadanie12 {
    public static void main(String[] args) {
        String text = "abecadlo123";
        printDigits(text);
    }

    public static void printDigits(String text) {
        for (int i = 0; i < text.length(); i++) {
            if (Character.isDigit(text.charAt(i))) {
                System.out.println(text.charAt(i));
            }
        }
    }
}
