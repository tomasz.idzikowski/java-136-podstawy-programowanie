package sda.java136;

import java.util.Stack;

//31.Napisz program, który zamieni liczbę całkowitą na binarną. Zapisz własny algorytm, następnie wykorzystaj implementację w standardowej bibliotece javy.
public class Zadanie31 {
    public static String getBinary(int n) {
        String wynik = " ";
        while (n != 0) {
            int reszta = n % 2;
            n = n / 2;
            wynik = wynik + reszta;
        }
        return new StringBuilder(wynik).reverse().toString();
    }

    public static String getBinaryWithStack(int n) {
        Stack<Integer> stack = new Stack<>();
        StringBuilder result = new StringBuilder("");
        while (n != 0) {
            stack.add(n % 2);
            n = n / 2;
        }
        while (!stack.empty()) {
            Integer element = stack.pop();
            result.append(element);
        }

        return result.toString();
    }

    public static void main(String[] args) {
        System.out.println(getBinary(40));
        System.out.println(getBinaryWithStack(40));
        System.out.println(Integer.toBinaryString((40)));
    }
}
