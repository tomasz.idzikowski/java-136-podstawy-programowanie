package sda.java136;

//19.Napisz program służący do rozwiązywania równania kwadratowego. Program powinien pobrać trzy liczby całkowite (współczynniki równania kwadratowego a, b, c i wyliczyć pierwiastki x1, x2
public class Zadanie19 {
    public static void main(String[] args) {
        obliczPierwiastki(1,-5,6);
    }

    public static void obliczPierwiastki(int a, int b, int c){
        System.out.println(a + "x^2 + " + b + "x + " + c);
        double delta = b*b-4*a*c;
        System.out.println("Wartosc delty: " + delta);
        if (delta < 0) {
            System.out.println("Delta ujemna - Brak rozwiazan rzeczywistych.");
        } else {
            double pierwiastekDelta = Math.sqrt(delta);
            double x1 = (-b - pierwiastekDelta) / (2 * a);
            double x2 = (-b + pierwiastekDelta) / (2 * a);
            System.out.printf("Pierwiastek x1: %.3f", x1);
            System.out.println();
            System.out.printf("Pierwiastek x2: %.3f", x2);
        }
    }
}
