package sda.java136;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//32. Napisz program, w którym zadeklarujesz tablicę Stringów. Przypis do elementów tablicy nazwy stolic państw. Sprawdz, która stolica wystąpiła najczęściej i ile razy.
public class Zadanie32 {
    public static void main(String[] args) throws IOException {
        getNations();
    }

    public static String getNations() throws IOException {
        List<String> plik = Files.readAllLines(Paths.get("capitalcity.txt"));
        Map<String, Integer> mapa = new HashMap<>();

        for (String line : plik) {
            if (mapa.containsKey(line)) {
                Integer value = mapa.get(line);
                mapa.replace(line, value + 1);
            } else {
                mapa.put(line, 1);
            }
        }
        int max = 0;
        String stolica = "";
        for (Map.Entry<String, Integer> entry : mapa.entrySet()) {
            if (entry.getValue() > max) {
                max = entry.getValue();
                stolica = entry.getKey();

            }

        }
        System.out.println(stolica);
        System.out.println(max);
        return stolica;
    }
}
