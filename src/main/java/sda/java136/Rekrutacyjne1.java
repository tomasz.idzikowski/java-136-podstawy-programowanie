package sda.java136;

import java.util.Objects;

public class Rekrutacyjne1 {
    public static String StringChallenge(String str) {
        str = str.replace(" ", "");
        if (Objects.isNull(str) || str.isEmpty()) {
            return "";
        }
        int counter = 1;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length() - 1; i++) {
            if (str.charAt(i) == str.charAt(i + 1)) {
                counter++;
            } else {
                sb.append(counter);
                sb.append(str.charAt(i));
                counter = 1;
            }
        }
        sb.append(counter);
        sb.append(str.charAt(str.length() - 1));
        return sb.toString();
    }

    public static void main(String[] args) {
        String s1 = "aabbcde";
        String s2 = "wwwbbbw";
        String s3 = "aaabb";
        String s4 = "   ";
        System.out.println(StringChallenge(s1));
        System.out.println(StringChallenge(s2));
        System.out.println(StringChallenge(s3));
        System.out.println(StringChallenge(s4));
    }
}
